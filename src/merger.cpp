#include <ros/ros.h>
#include <string.h>
#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include "sensor_msgs/LaserScan.h"
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

double alturaFiltroInferior = 0.1;
double alturaFiltroSuperior = 3.0;
double distanciaMinimaValida = 0.1;

class LaserscanMerger
{
  public:
    LaserscanMerger(){
      scan_subscriber1 = node.subscribe("pico_scan", 1, &LaserscanMerger::laserCallback, this);
      scan_subscriber2 = node.subscribe("astra_scan", 1, &LaserscanMerger::laserCallback, this);
      laser_scan_publisher = node.advertise<sensor_msgs::LaserScan> ("merged_lasers", 1, false);
    }

  private:
      ros::NodeHandle node;
      tf::TransformListener tfListener;
      tf::Transform transform;

      ros::Publisher laser_scan_publisher;
      ros::Subscriber scan_subscriber1;
      ros::Subscriber scan_subscriber2;

      sensor_msgs::LaserScan scanToPublish;
      // scanToPublish.angle_min = - M_PI;
      // scanToPublish.angle_max = M_PI;
      // scanToPublish.angle_increment = 0.00436332313;
      // scanToPublish.range_min = 0.5;
      // scanToPublish.range_max = 7.0;
      // // scan.ranges.resize( (M_PI*2)/0.00436332313);

      string destination_frame;
      string scan_destination_topic;

  void laserCallback(const sensor_msgs::LaserScan& msg){
    this->scanToPublish = msg;
    this->scanToPublish.header.frame_id = "base_link";
    if(msg.header.frame_id == "camera_link"){
      this->scanToPublish.angle_min = 0.8975;
      this->scanToPublish.angle_max = 1.9371;
    }else{
      this->scanToPublish.angle_min = 1.15;
      this->scanToPublish.angle_max = 2.1971;
    }

    this->scanToPublish.ranges = msg.ranges;


    std::vector<float> nuevosRangos(msg.ranges.size());
    for(int i = 0; i != msg.ranges.size(); i++){
			float rangoOriginal = msg.ranges[i];
			float rangoFinal = rangoOriginal;
			double anguloPunto = (i*msg.angle_increment) + msg.angle_min;
			double xPunto = msg.ranges[i] * cos(anguloPunto);
			double yPunto = msg.ranges[i] * sin(anguloPunto);


			geometry_msgs::PointStamped laser_point;
			laser_point.header.frame_id = msg.header.frame_id;
			laser_point.header.stamp = ros::Time(0);

			//just an arbitrary point in space
			laser_point.point.x = xPunto;
			laser_point.point.y = yPunto;
			laser_point.point.z = 0.0;


			try{
				geometry_msgs::PointStamped laser_footprint_point;

				tfListener.transformPoint("base_link", laser_point, laser_footprint_point);

				if(rangoFinal > distanciaMinimaValida){
					nuevosRangos[i] = rangoFinal;
				}else{
					nuevosRangos[i] = scanToPublish.range_max;
				}
			}
			catch (tf::TransformException ex){
				ROS_INFO_THROTTLE(0.5, "%s",ex.what());
			}
		}

    scanToPublish.ranges = nuevosRangos;
    laser_scan_publisher.publish(scanToPublish);
  }
};


int main(int argc, char **argv)
{

	//inicializar ros
	ros::init(argc, argv, "MultiLaser Merger");

	LaserscanMerger laser_merger;

  // Ratio de iteración (Hz)
  ros::spin();
	return 0;
};
